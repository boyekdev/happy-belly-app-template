var gulp = require('gulp');
var sass = require('gulp-sass');
var bs = require('browser-sync').create();

gulp.task('browser-sync', ['sass'], function () {
    bs.init({
        server: {
            baseDir: "./"
        }
    });
});

gulp.task('sass', function () {
    return gulp.src('app/assets/styles/scss/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('app/assets/styles/css/'))
        .pipe(bs.reload({
            stream: true
        }));
});

gulp.task('watch', ['browser-sync'], function () {
    gulp.watch("app/assets/styles/scss/*.scss", ['sass']);
    gulp.watch("app/*.html").on('change', bs.reload);
});
